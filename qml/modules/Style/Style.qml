/******************************************************************************
**
** File      Style.qml
** Author    Andrii Sydorenko
**
******************************************************************************/

pragma Singleton

import QtQuick 2.0

QtObject {
    readonly property Palette palette: Palette{}
    readonly property ButtonStyle button: ButtonStyle{}
    readonly property TextStyle text: TextStyle{}
}
