/******************************************************************************
**
** File      TButtonStyle.qml
** Author    Andrii Sydorenko
**
******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls.Material

QtObject {

    property int fontSizeRegular: 16
    property int fontSizeTitle: 20

    property int containerStyle: Material.Filled
}
