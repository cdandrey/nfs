/******************************************************************************
**
** File      TPalette.qml
** Author    Andrii Sydorenko
**
******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls.Material

QtObject {

    property int dark: Material.Dark
    property int light: Material.Light

    property int backgroundDark: Material.Green
    property int backgroundLight: Material.DeepPurple

    property int foregroundDark: Material.DeepPurple
    property int foregroundLight: Material.Green

    property int titleSize: 18
    property bool titleBold: true

    property int subTitleSize: 16
    property bool subTitleBold: true

    property int regularSize: 14
    property bool regularBold: false
}
