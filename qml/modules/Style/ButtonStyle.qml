/******************************************************************************
**
** File      TButtonStyle.qml
** Author    Andrii Sydorenko
**
******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls.Material

QtObject {

    readonly property int background: Material.Green
    readonly property int foreground: Material.DeepPurple

    readonly property int width: 200
    readonly property int widthShort: 100

    readonly property int height: 50

    readonly property int fontSize: 16
    readonly property int padding: 5

    readonly property int roundedScale: Material.MediumScale

    readonly property string iconBack: "qrc:/assets/icons/back.png"
    readonly property string iconDrawer: "qrc:/assets/icons/drawer.png"
    readonly property string iconMenu: "qrc:/assets/icons/menu.png"
    readonly property string iconNext: "qrc:/assets/icons/next.png"
}
