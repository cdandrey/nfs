import QtQuick 2.15
import QtQuick.Layouts

Item {
    id: root

    signal turn()
    signal stop()
    signal stopped()

    property int value: -1

    onValueChanged: {
        side.tryTurn(!headToTail.running && !tailToHead.running)
    }

    property bool headFlag: true

    MouseArea {
        anchors.fill: parent

//        onClicked: {
//            side.tryTurn(!headToTail.running && !tailToHead.running)
//        }
//
        onPressed: {
            turn()
        }

        onReleased: {
            stop()
        }
    }

    Item {
        id: side

        anchors.centerIn: parent

        property int currentValue: 1
        property int prevValue: 1

        width: root.width/3
        height: root.width/3

        function increment(val) {
            return (val + 1) % 2;
        }

        function toTurn() {
            prevValue = currentValue
            currentValue = increment(currentValue)
        }

        function tryTurn(transitionIsFinished) {
            if (transitionIsFinished && (root.value === -1)) {
                toTurn()
            } else if (transitionIsFinished && (root.value != currentValue)) {
                toTurn()
            } else if (transitionIsFinished) {
                stopped()
            }
        }

        Image {
            id: head

            property real angle: headRotation.angle

            source: "qrc:/assets/imgs/coin_head.png"

            z: 1

            transform: Rotation {
                id: headRotation
                axis { x: 0; y: 1; z: 0 }
                origin.x: head.width/2
                origin.y: head.height/2
                angle: 0
            }
        }

        Image {
            id: tail

            property real angle: tailRotation.angle

            source: "qrc:/assets/imgs/coin_tail.png"

            z: -1

            transform: Rotation {
                id: tailRotation
                axis { x: 0; y: 1; z: 0 }
                origin.x: head.width/2
                origin.y: head.height/2
                angle: 180
            }
        }

        states: [
            State {
                name: "head"
                when: side.prevValue === 0 && side.currentValue === 0
                PropertyChanges {
                    target: headRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: tailRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "headToTail"
                when: side.prevValue === 0 && side.currentValue === 1
                PropertyChanges {
                    target: head
                    restoreEntryValues: false
                    explicit: true
                    z: -1
                }
                PropertyChanges {
                    target: headRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
                PropertyChanges {
                    target: tail
                    restoreEntryValues: false
                    explicit: true
                    z: 1
                }
                PropertyChanges {
                    target: tailRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 360
                }
            },
            State {
                name: "tail"
                when: side.prevValue === 1 && side.currentValue === 1
                PropertyChanges {
                    target: headRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
                PropertyChanges {
                    target: tailRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
            },
            State {
                name: "tailToHead"
                when: side.prevValue === 1 && side.currentValue === 0
                PropertyChanges {
                    target: head
                    restoreEntryValues: false
                    explicit: true
                    z: 1
                }
                PropertyChanges {
                    target: headRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 360
                }
                PropertyChanges {
                    target: tail
                    restoreEntryValues: false
                    explicit: true
                    z: -1
                }
                PropertyChanges {
                    target: tailRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            }
        ]

        transitions: [
            Transition {
                id: headToTail
                to: "headToTail"
                PropertyAnimation {
                    targets: [head,headRotation,tail,tailRotation]
                    properties: "angle,z"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryTurn(!running);
                }
            },
            Transition {
                id: tailToHead
                to: "tailToHead"
                PropertyAnimation {
                    targets: [head,headRotation,tail,tailRotation]
                    properties: "angle,z"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryTurn(!running);
                }
            }
        ]
    }
}
