import QtQuick 2.15
import QtQuick.Controls.Material

Item {

    id: root

    property color markColor: Qt.black
    property color sideColor: Qt.white
    property int mark: 6
    property bool mirrored: false

    Rectangle {

        id: side

        anchors.fill: parent

        border.width: 2
        border.color: root.markColor
        color: root.sideColor
        radius: width/6
        antialiasing: true

        readonly property double sidePadding: width < height ? width/20 : height/20
        readonly property double markPadding: 10
        readonly property double markRadius: width < height ? (width - 2*sidePadding)/6 - markPadding : (height - 2*sidePadding)/6 - markPadding

        readonly property double xLeft: width/6 + sidePadding - markRadius
        readonly property double xCenter: 3*width/6 - markRadius
        readonly property double xRight: 5*width/6 - sidePadding - markRadius

        readonly property double yTop: height/6 + sidePadding - markRadius
        readonly property double yCenter: 3*height/6 - markRadius
        readonly property double yBottom: 5*height/6 - sidePadding - markRadius

        readonly property var centerPointsTwo: mirrored ? [Qt.point(xRight,yTop),Qt.point(xLeft,yBottom)]
                                                        : [Qt.point(xLeft,yTop),Qt.point(xRight,yBottom)]

        readonly property var centerPointsThree: mirrored ? [Qt.point(xRight,yTop),Qt.point(xCenter,yCenter),Qt.point(xLeft,yBottom)]
                                                        : [Qt.point(xLeft,yTop),Qt.point(xCenter,yCenter),Qt.point(xRight,yBottom)]

        readonly property var centerPoints: [
            [Qt.point(xCenter,yCenter)],
            centerPointsTwo, centerPointsThree,
            [Qt.point(xLeft,yTop),Qt.point(xRight,yTop),Qt.point(xLeft,yBottom),Qt.point(xRight,yBottom)],
            [Qt.point(xLeft,yTop),Qt.point(xRight,yTop),Qt.point(xCenter,yCenter),Qt.point(xLeft,yBottom),Qt.point(xRight,yBottom)],
            [Qt.point(xLeft,yTop),Qt.point(xLeft,yCenter),Qt.point(xLeft,yBottom),
             Qt.point(xRight,yTop),Qt.point(xRight,yCenter),Qt.point(xRight,yBottom)]
        ]

        Repeater {
            model: side.centerPoints[root.mark - 1]
            Rectangle {
                x: modelData.x
                y: modelData.y
                color: root.markColor
                width: side.markRadius*2
                height: side.markRadius*2
                radius: side.markRadius
                antialiasing: true
            }
        }
    }
}
