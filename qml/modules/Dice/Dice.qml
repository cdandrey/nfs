import QtQuick 2.15
import QtQuick.Layouts

Item {
    id: root

    property color markColor: Qt.black
    property color sideColor: Qt.white

    property int value: 6
    property int initValue: 6

    signal roll()
    signal stop()
    signal stopped()

    onValueChanged: {
        side.tryRoll(!oneTwo.running && !twoThree.running && !threeFour.running
                && !fourFive.running && !fiveSix.running && !sixOne.running)
    }

    Item {
        id: side

        anchors.centerIn: parent

        property int currentValue: root.initValue
        property int prevValue: root.initValue

        width: root.width/3
        height: width

        function increment(val) {
            return Math.max(1,(val + 1) % 7);
        }

        function toRoll() {
            prevValue = currentValue
            currentValue = increment(currentValue)
        }

        function tryRoll(transitionIsFinished) {
            if (transitionIsFinished && (root.value === 0)) {
                toRoll()
            } else if (transitionIsFinished && (root.value != currentValue)) {
                toRoll()
            } else if (transitionIsFinished) {
                stopped()
            }
        }

        MouseArea {
            anchors.fill: parent

            onPressed: {
                root.roll()
            }

            onReleased: {
                root.stop()
            }
        }

        DiceSide {
            id: sideOne
            mark: 1

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            x: 0
            y: 0

            transform: Rotation {
                id: sideOneRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 90
            }
        }

        DiceSide {
            id: sideTwo
            mark: 2

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            mirrored: false

            x: 0
            y: 0

            transform:  Rotation {
                id: sideTwoRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 90
            }
        }

        DiceSide {
            id: sideThree
            mark: 3

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            mirrored: false

            x: 0
            y: 0

            transform:  Rotation {
                id: sideThreeRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 90
            }
        }

        DiceSide {
            id: sideFour
            mark: 4

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            mirrored: false

            x: 0
            y: 0

            transform:  Rotation {
                id: sideFourRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 90
            }
        }

        DiceSide {
            id: sideFive
            mark: 5

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            mirrored: false

            x: 0
            y: 0

            transform:  Rotation {
                id: sideFiveRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 90
            }
        }

        DiceSide {
            id: sideSix
            mark: 6

            width: side.width
            height: side.height

            markColor: root.markColor
            sideColor: root.sideColor

            x: 0
            y: 0

            transform: Rotation {
                id: sideSixRotation
                origin.x: 0
                origin.y: 0
                axis { x: 0; y: 1; z: 0 }
                angle: 0
            }
        }

        states: [
            State {
                name: "11"
                when: side.prevValue === 1 && side.currentValue === 1
                PropertyChanges {
                    target: sideOne
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
                PropertyChanges {
                    target: sideOneRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: sideTwo
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                    mirrored: false
                }
            },
            State {
                name: "12"
                when: side.prevValue === 1 && side.currentValue === 2
                PropertyChanges {
                    target: sideOne
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideOneRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
                PropertyChanges {
                    target: sideTwo
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideTwoRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "22"
                when: side.prevValue === 2 && side.currentValue === 2
                PropertyChanges {
                    target: sideTwo
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                    mirrored: true
                }
                PropertyChanges {
                    target: sideTwoRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: sideThree
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                    mirrored: false
                }
            },
            State {
                name: "23"
                when: side.prevValue === 2 && side.currentValue === 3
                PropertyChanges {
                    target: sideTwo
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideTwoRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
                PropertyChanges {
                    target: sideThree
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideThreeRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "33"
                when: side.prevValue === 3 && side.currentValue === 3
                PropertyChanges {
                    target: sideThree
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                    mirrored: true
                }
                PropertyChanges {
                    target: sideThreeRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: sideFour
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
            },
            State {
                name: "34"
                when: side.prevValue === 3 && side.currentValue === 4
                PropertyChanges {
                    target: sideThree
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideThreeRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
                PropertyChanges {
                    target: sideFour
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideFourRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "44"
                when: side.prevValue === 4 && side.currentValue === 4
                PropertyChanges {
                    target: sideFour
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
                PropertyChanges {
                    target: sideFourRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: sideFive
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
            },
            State {
                name: "45"
                when: side.prevValue === 4 && side.currentValue === 5
                PropertyChanges {
                    target: sideFour
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideFourRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
                PropertyChanges {
                    target: sideFive
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideFiveRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "55"
                when: side.prevValue === 5 && side.currentValue === 5
                PropertyChanges {
                    target: sideFive
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
                PropertyChanges {
                    target: sideFiveRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
                PropertyChanges {
                    target: sideSix
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
            },
            State {
                name: "56"
                when: side.prevValue === 5 && side.currentValue === 6
                PropertyChanges {
                    target: sideFive
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideFiveRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
                PropertyChanges {
                    target: sideSix
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideSixRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
            },
            State {
                name: "66"
                when: side.prevValue === 6 && side.currentValue === 6
                PropertyChanges {
                    target: sideOne
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
                PropertyChanges {
                    target: sideSix
                    restoreEntryValues: false
                    explicit: true
                    x: 0
                }
                PropertyChanges {
                    target: sideSixRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 0
                }
            },
            State {
                name: "61"
                when: side.prevValue === 6 && side.currentValue === 1
                PropertyChanges {
                    target: sideOne
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideOneRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 180
                }
                PropertyChanges {
                    target: sideSix
                    restoreEntryValues: false
                    explicit: true
                    x: width
                }
                PropertyChanges {
                    target: sideSixRotation
                    restoreEntryValues: false
                    explicit: true
                    angle: 90
                }
            }
        ]

        transitions: [
            Transition {
                id: oneTwo
                to: "12"
                PropertyAnimation {
                    targets: [sideOne,sideOneRotation,sideTwo,sideTwoRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            },
            Transition {
                id: twoThree
                to: "23"
                PropertyAnimation {
                    targets: [sideTwo,sideTwoRotation,sideThree,sideThreeRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            },
            Transition {
                id: threeFour
                to: "34"
                PropertyAnimation {
                    targets: [sideThree,sideThreeRotation,sideFour,sideFourRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            },
            Transition {
                id: fourFive
                to: "45"
                PropertyAnimation {
                    targets: [sideFour,sideFourRotation,sideFive,sideFiveRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            },
            Transition {
                id: fiveSix
                to: "56"
                PropertyAnimation {
                    targets: [sideFive,sideFiveRotation,sideSix,sideSixRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            },
            Transition {
                id: sixOne
                to: "61"
                PropertyAnimation {
                    targets: [sideOne,sideOneRotation,sideSix,sideSixRotation]
                    properties: "angle,x"
                    duration: 200
                    easing.type: Easing.InOutQuad
                }
                onRunningChanged: {
                    side.tryRoll(!running);
                }
            }
        ]
    }
}
