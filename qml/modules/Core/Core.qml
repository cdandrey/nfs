/******************************************************************************
**
** File      Core.qml
** Author    Andrii Sydorenko
**
******************************************************************************/

pragma Singleton

import QtQuick 2.0

QtObject {

    readonly property var info: [
        "",										// MainMenu
        "Input request",						// InputRequest
        "Press and reales to roll the dice",	// ApproveRequest
        "Request rejected",						// RequestRejected
        "Choose card",							// GoChooseCard
        "Press and reales to roll the dice",	// ChooseCard
        "Choose side of the card",				// GoChooseSideCard
        "Press and reales to flip a coin",		// ChooseSideCard
        "View card",							// GoViewCard
        "Input next request"					// InputRequest
    ]

    readonly property string title: "Notes of the French Sibyl"

    function state() {

        if (coreModel) {
            return coreModel.state
        }

        console.log("Error: Call Core::state for undefined CoreModel")
        return -1
    }

    function stateIs(state) {

        if (coreModel) {
            return coreModel.state === state
        }

        console.log("Error: Call Core::stateIs for undefined CoreModel")
        return false
    }

    function currentRequest() {

        if (coreModel && coreModel.requestModel) {
            return coreModel.requestModel.currentRequest
        }

        return ""
    }

    function diceValue() {

        if (coreModel && coreModel.diceModel) {
            return coreModel.diceModel.value
        }

        return 0
    }

    function diceInitValue() {

        if (coreModel && coreModel.diceModel) {
            return coreModel.diceModel.initValue
        }

        return 0
    }

    function coinValue() {

        if (coreModel && coreModel.coinModel) {
            return coreModel.coinModel.value
        }

        return 0
    }

    function coinInitValue() {

        if (coreModel && coreModel.coinModel) {
            return coreModel.coinModel.initValue
        }

        return 0
    }

}
