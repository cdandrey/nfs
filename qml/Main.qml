/******************************************************************************
**
** File      MainWindow.qml
** Author    Andrii Sydorenko
**
** This file is part of SMT - Smart Toolbox
**
******************************************************************************/

import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQml.Models

import Style 1.0
import Core 1.0
import Dice 1.0
import Coin 1.0
import "pages"

ApplicationWindow {
	id: root

    objectName: "MainWindow"

    readonly property var pages: [mainMenuPage,
                                  inputRequestPage,	// InputRequest
                                  dicePage,         // ApproveRequest
                                  false,	        // RejectRequest
                                  false,	        // GoChooseCard
                                  dicePage,	        // ChooseCard
                                  false,	        // GoChooseSideCard
                                  coinPage,	        // ChooseSideCard
                                  false,	        // GoViewCard
                                  false 	        // ViewCard
    ]

    property bool diceRolling: false
    property bool coinTurning: false

    Material.theme: Style.palette.light
    Material.primary: Style.palette.backgroundDark
    Material.background: Style.palette.backgroundLight

    flags: Qt.Window | Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint

	visible: true

    width: 1200
    height: 900

    signal startGame()
    signal requestAccepted(request: string)
    signal diceRoll()
    signal diceStop()
    signal coinTurn()
    signal coinStop()
    signal actionBack()
    signal actionNext()

    function onCoreStateChanged () {
        if (pages[Core.state()]) {
            stackView.clear(StackView.Immediate)
            stackView.push(pages[Core.state()])
        }
    }

    MouseArea {
		id: mouseArea

		property point mousePosPressed: Qt.point(0, 0)

		anchors.fill: parent

		onPositionChanged: {
			if (!mouseArea.pressed) return;

			var pos = mapToGlobal(mouseArea.mouseX,mouseArea.mouseY);
			
			Window.window.x = (pos.x - mousePosPressed.x);
			Window.window.y = (pos.y - mousePosPressed.y);
		}

		onPressed: mousePosPressed = Qt.point(mouseX, mouseY)
	}

    Action {
        id: optionsMenuAction
        icon.source: Style.button.iconMenu
        onTriggered: optionsMenu.open()
    }

    header: ToolBar {
        
        Material.primary: Style.palette.backgroundDark
        Material.foreground: Style.palette.foregroundDark

        visible: !Core.stateIs(CoreState.MainMenu)

        RowLayout {

            anchors.fill: parent

            // Title
            Label {
                Layout.fillWidth: true
                Layout.leftMargin: 10

                text: qsTr(Core.stateIs(CoreState.InputRequest) ? Core.title : Core.currentRequest())

                font.bold: true
                font.pixelSize: Style.palette.titleSize
            }

            ToolButton {
                action: optionsMenuAction
                Menu {
                    Material.theme: Style.palette.dark
                    Material.background: Style.button.background
                    Material.foreground: Style.button.foreground

                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    Action {
                        text: "Settings"
                        onTriggered: {
                            stackView.pop();
                            stackView.push(mainMenuPage)
                        }
                    }
                    Action {
                        text: "Help"
                        onTriggered: {
                        }
                    }
                    Action {
                        text: "Exit"
                        onTriggered: close()
                    }
                }
            }
        }
    }

    footer: ToolBar {

        Material.primary: Style.palette.backgroundDark
        Material.foreground: Style.palette.foregroundDark

        visible: !Core.stateIs(CoreState.MainMenu)

        RowLayout {

            anchors.fill: parent

            ToolButton {
                id: buttonBackFooter
                icon.source: Style.button.iconBack
                visible: !diceRolling && !coinTurning && Core.stateIs(CoreState.RejectRequest)

                onClicked: {
                    actionBack()
                }
            }

            Item {
                Layout.fillWidth: true
            }

            Label {
                id: labelInfoFooter
                Layout.leftMargin: 10
                text: Core.info[Core.state()]
                font.pixelSize: Style.palette.titleSize
                visible: !diceRolling && !coinTurning
            }

            Item {
                Layout.fillWidth: true
            }

            ToolButton {
                id: buttonNextFooter
                icon.source: Style.button.iconNext
                visible: !diceRolling && !coinTurning &&
                         (Core.stateIs(CoreState.InputRequest) |
                          Core.stateIs(CoreState.GoChooseCard) |
                          Core.stateIs(CoreState.GoChooseSideCard) |
                          Core.stateIs(CoreState.GoViewCard) |
                          Core.stateIs(CoreState.ViewCard))

                onClicked: {
                    actionNext()
                }
            }
        }
    }

    StackView {
        id: stackView

        anchors.fill: parent

        initialItem: pages[Core.state()]
    }

    Component {
        id: mainMenuPage

        MainMenu {

            onExitClicked: {
                close()
            }

            onStartGameClicked: {
                startGame()
            }
        }
    }

    Component {
        id: inputRequestPage

        InputRequest {

            Material.theme:  Style.palette.light
            Material.background: Style.palette.backgroundDark
            Material.foreground: Style.palette.foregroundDark

            Connections {
                target: buttonNextFooter
                function onClicked() { requestAccepted(textRequest); }
                enabled: true
            }
        }
    }

    Component {
        id: dicePage

        Dice {

            Material.theme:  Style.palette.light
            Material.background: Style.palette.backgroundDark
            Material.foreground: Style.palette.foregroundDark

            markColor: Material.color(Style.palette.backgroundLight,Style.palette.dark)
            sideColor: Material.color(Style.palette.backgroundLight)

            value: Core.diceValue()
            initValue: Core.diceInitValue()
            enabled: Core.stateIs(CoreState.ApproveRequest) | Core.stateIs(CoreState.ChooseCard)

            onRoll: {
                diceRoll()
                diceRolling = true
            }

            onStop: {
                diceStop()
            }

            onStopped: {
                diceRolling = false
            }
        }
    }

    Component {
        id: coinPage

        Coin {

            Material.theme:  Style.palette.light
            Material.background: Style.palette.backgroundDark
            Material.foreground: Style.palette.foregroundDark

            value: Core.coinValue()
            enabled: Core.stateIs(CoreState.ChooseSideCard)

            onTurn: {
                coinTurn()
                coinTurning = true
            }

            onStop: {
                coinStop()
            }

            onStopped: {
                coinTurning = false
            }
        }
    }
}
