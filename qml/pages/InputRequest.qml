/******************************************************************************
**
** File      MainWindow.qml
** Author    Andrii Sydorenko
**
** This file is part of SMT - Smart Toolbox
**
******************************************************************************/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material

import Style 1.0

ColumnLayout {

    spacing: 0

    property alias textRequest: textFieldRequest.text

    Item {
        Layout.fillHeight: true
    }

    TextField {
        id: textFieldRequest

        Material.containerStyle: Style.text.containerStyle

        Layout.fillWidth: true
        Layout.alignment: Qt.AlignCenter
        Layout.leftMargin: parent.width * 0.05
        Layout.rightMargin: parent.width * 0.05

        placeholderText: qsTr("Input request")
        font.pointSize: Style.text.fontSizeTitle

        onActiveFocusChanged: {
            if (activeFocus || text !== "") {
                placeholderText = "";
            } else {
                placeholderText = qsTr("Input request");
            }
        }

        onAccepted: {
            requestAccepted(text)
        }
    }

    Item {
        Layout.fillHeight: true
    }
}
