/******************************************************************************
**
** File      MainWindow.qml
** Author    Andrii Sydorenko
**
** This file is part of SMT - Smart Toolbox
**
******************************************************************************/

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material

import Style 1.0

ColumnLayout {

    id: root

    objectName: "MainMenuWindow"

    spacing: 0

    signal startGameClicked()
    signal exitClicked()

    Label {
        Material.foreground: Style.palette.foregroundLight
        text: "Notes of the French Sibyl"
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        Layout.topMargin: root.height * 0.05
        font.pixelSize: Style.palette.titleSize
        font.bold: Style.palette.titleBold
    }

    Item {
        Layout.fillHeight: true
    }

    Repeater {
        model: [
            { "caption": "Start Game", "handler": "startGameClicked", "enabled": true },
            { "caption": "Exit", "handler": "exitClicked", "enabled": true },
        ]

        Button {

            Material.theme:  Style.palette.light
            Material.background: Style.button.background
            Material.foreground: Style.button.foreground

            Layout.preferredWidth: Style.button.width
            Layout.preferredHeight: Style.button.height
            Layout.alignment: Qt.AlignCenter

            focusPolicy: Qt.NoFocus
            text: modelData.caption
            enabled: modelData.enabled
            font.pixelSize: Style.button.fontSize

            onClicked: root[modelData.handler]()
        }
    }

    Item {
        Layout.fillHeight: true
    }
}
