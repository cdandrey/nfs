
#include "RandomizeModel.h"
#include "../utils/Randomizer.h"

#include <iostream>
#include <future>
#include <thread>

namespace Nfs {

RandomizeModel::RandomizeModel(unsigned int minValue, unsigned int maxValue)
    : _randomizer{std::make_unique<utils::Randomizer>(minValue,maxValue)}
    , _value{maxValue}
{
}

void RandomizeModel::onGenerate()
{
    // Set the value outside the range, which will rotate the visual object until a random value is generated
    _value = _randomizer->getMaxValue() + 1;
	emit valueChanged();

	const auto generateValue = [this]() {
        _isGenerating = true;
        while (_isGenerating) {
            _value = _randomizer->generateValue();
		}

		emit valueChanged();
        emit valueGenerated();
	};

	std::thread{generateValue}.detach();
}

void RandomizeModel::onStop()
{
    _isGenerating = false;
}

void RandomizeModel::onReset()
{
    _value = getInitValue();
}

unsigned int RandomizeModel::getValue() const
{
    return _value;
}

unsigned int RandomizeModel::getMaxValue() const
{
    return _randomizer->getMaxValue();
}

unsigned int RandomizeModel::getInitValue() const
{
    return getMaxValue();
}

}	// namespace Nfs
