#pragma once

#include "../data/Request.h"

#include <QAbstractListModel>
#include <QTime>

namespace Nfs
{

namespace Model
{

class RequestModel : public QAbstractListModel
{
	Q_OBJECT
    Q_PROPERTY(QString currentRequest READ currentRequest NOTIFY requestChanged)

public:
    enum class Role
    {
        Request = Qt::UserRole,
        Card
    };

    explicit RequestModel(QObject *parent = nullptr);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    int requestCount() const;
    QString currentRequest() const;

public slots:

    void onRequestAppend(QString request);
    void onRequestRejected();

signals:

    void requestChanged();

private:

    QList<Data::Request> _data;
};

}	// namespace Model

}  // namespace Nfs
