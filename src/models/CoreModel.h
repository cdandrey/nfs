#pragma once

#include "RandomizeModel.h"
#include "RequestModel.h"

#include <QObject>
#include <memory>

namespace Nfs
{

namespace Model
{

Q_NAMESPACE
enum class State
{
    MainMenu,
    InputRequest,
    ApproveRequest,
    RejectRequest,
    GoChooseCard,
    ChooseCard,
    GoChooseSideCard,
    ChooseSideCard,
    GoViewCard,
    ViewCard
};
Q_ENUM_NS(State)


class CoreModel : public QObject
{
	Q_OBJECT
    Q_PROPERTY(State state MEMBER _state NOTIFY stateChanged)
    Q_PROPERTY(RequestModel *requestModel READ requestModel CONSTANT)
    Q_PROPERTY(RandomizeModel *diceModel READ diceModel CONSTANT)
    Q_PROPERTY(RandomizeModel *coinModel READ coinModel CONSTANT)

public:
	explicit CoreModel(QObject *parent = nullptr);

    RequestModel* requestModel() const;
    RandomizeModel* diceModel() const;
    RandomizeModel* coinModel() const;

public slots:

    void onUpdateState();

signals:

    void requestRejected();
    void stateChanged();

private:

    void moveToState(State state);

    State _state = State::MainMenu;

    std::unique_ptr<RequestModel> _requestModel;
    std::unique_ptr<RandomizeModel> _diceModel;
    std::unique_ptr<RandomizeModel> _coinModel;
};

}	// namespace Model

}	// namespace Nfs
