#pragma once

#include <chrono>
#include <queue>
#include <QObject>

namespace Nfs {

namespace utils
{
	class Randomizer;
}

class RandomizeModel : public QObject
{
	Q_OBJECT
    Q_PROPERTY(unsigned value READ getValue NOTIFY valueChanged)
    Q_PROPERTY(unsigned initValue READ getInitValue CONSTANT)

public:
    RandomizeModel(unsigned minValue, unsigned maxValue);
    ~RandomizeModel() = default;

    unsigned getValue() const;
    unsigned getMaxValue() const;
    unsigned getInitValue() const;

public slots:

    void onGenerate();
    void onStop();
    void onReset();

signals:

	void valueChanged();
    void valueGenerated();

private:

	std::unique_ptr<utils::Randomizer> _randomizer;
    unsigned _value;
    bool _isGenerating;
};

}
