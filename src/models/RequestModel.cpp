#include "RequestModel.h"
#include "../data/Request.h"

namespace Nfs {

namespace Model
{

RequestModel::RequestModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int RequestModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
		return 0;

    return _data.size();
}

QVariant RequestModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= _data.size()) {
        return {};
    }

    const auto& item = _data[index.row()];

	switch (static_cast<Role>(role)) {
    case Role::Request:
        return item.request;
    case Role::Card:
        return item.cards.last().img;
	}

    return {};
}

QHash<int, QByteArray> RequestModel::roleNames() const
{
    return {{static_cast<int>(Role::Request), "request"}, {static_cast<int>(Role::Card), "card"}};
}

int RequestModel::requestCount() const
{
    return _data.size();
}

QString RequestModel::currentRequest() const
{
    return _data.size() > 0 ? _data.last().request : "";
}

void RequestModel::onRequestAppend(QString request)
{
    if (!request.isEmpty())
    {
        _data.append(Data::Request{request});
        emit requestChanged();
    }
}

void RequestModel::onRequestRejected()
{
    _data.removeLast();
    emit requestChanged();
}

}	// namespace Model

}  // namespace Nfs
