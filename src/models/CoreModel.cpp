#include "CoreModel.h"

namespace Nfs {

namespace Model
{

CoreModel::CoreModel(QObject *parent)
    : QObject(parent)
    , _requestModel{std::make_unique<RequestModel>()}
    , _diceModel{std::make_unique<RandomizeModel>(1,6)}
    , _coinModel{std::make_unique<RandomizeModel>(0,1)}
{
    QObject::connect(_requestModel.get(),&RequestModel::requestChanged,this,&CoreModel::onUpdateState);
    QObject::connect(_diceModel.get(),&RandomizeModel::valueGenerated,this,&CoreModel::onUpdateState);
    QObject::connect(_coinModel.get(),&RandomizeModel::valueGenerated,this,&CoreModel::onUpdateState);

    QObject::connect(this,&CoreModel::requestRejected,_requestModel.get(),&RequestModel::onRequestRejected);
    QObject::connect(this,&CoreModel::stateChanged,_diceModel.get(),&RandomizeModel::onReset);
    QObject::connect(this,&CoreModel::stateChanged,_coinModel.get(),&RandomizeModel::onReset);
}

RequestModel *CoreModel::requestModel() const
{
	return _requestModel.get();
}

RandomizeModel *CoreModel::diceModel() const
{
    return _diceModel.get();
}

RandomizeModel *CoreModel::coinModel() const
{
    return _coinModel.get();
}

void CoreModel::onUpdateState()
{
    switch (_state) {
    case State::MainMenu:
        moveToState(State::InputRequest);
        break;
    case State::InputRequest:
        if (_requestModel->requestCount() == 1)
        {
            moveToState(State::ApproveRequest);
        }
        else
        {
            moveToState(State::GoChooseCard);
        }
        break;
    case State::RejectRequest:
        moveToState(State::InputRequest);
        break;
    case State::ApproveRequest:
        //if (_diceModel->getValue() == _diceModel->getMaxValue())
        if (_diceModel->getValue() % 2)
        {
            moveToState(State::GoChooseCard);
        }
        else
        {
            emit requestRejected();
            moveToState(State::RejectRequest);
        }
        break;
    case State::GoChooseCard:
        moveToState(State::ChooseCard);
        break;
    case State::ChooseCard:
        moveToState(State::GoChooseSideCard);
        break;
    case State::GoChooseSideCard:
        moveToState(State::ChooseSideCard);
        break;
    case State::ChooseSideCard:
        moveToState(State::GoViewCard);
        break;
    case State::GoViewCard:
        moveToState(State::ViewCard);
        break;
    case State::ViewCard:
        // if card.next == null -> InputRequest else -> view card.next
        //moveToState(State::ViewCard);
        break;
    default:
        break;
    }
}

void CoreModel::moveToState(State state)
{
    if (state != _state)
    {
        _state = state;
        emit stateChanged();
    }
}

}	// namespace Model

}	// namespace Nfs
