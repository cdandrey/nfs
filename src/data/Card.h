#pragma once

#include <QString>

namespace Nfs
{

namespace Data {

struct Card
{
    QString img;
    QString description;
    int nextCard;
    int side;
};

}

}

