#pragma once

#include "Card.h"

#include <QList>

namespace Nfs
{

namespace Data
{

struct Request
{

    explicit Request(QString _request = {});
    QString request;
    QList<Card> cards;
};

}	// namespace Data

}  // namespace smt
