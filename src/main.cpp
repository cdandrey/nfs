/******************************************************************************
**
** File      main.cpp
** Author    Andrii Sydorenko
**
******************************************************************************/

#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QResource>

#include <src/models/CoreModel.h>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

	QGuiApplication app(argc, argv);

    std::unique_ptr<Nfs::Model::CoreModel> coreModel = std::make_unique<Nfs::Model::CoreModel>();

    app.setWindowIcon(QIcon {"qrc:/assets/icons/name.ico"});

    qmlRegisterUncreatableMetaObject(Nfs::Model::staticMetaObject,"Core",1,0,"CoreState","Access to enums & flags only");
    QResource::registerResource(QCoreApplication::applicationDirPath().append("/nfs_resources.bin"));

    QQmlApplicationEngine qmlEngine;

    qmlEngine.rootContext()->setContextProperty("coreModel", coreModel.get());
    qmlEngine.addImportPath(QStringLiteral("qrc:/qml/modules"));

    const QUrl url(QStringLiteral("qrc:/qml/Main.qml"));
    qmlEngine.load(url);

    QObject::connect(
        &qmlEngine,
        &QQmlApplicationEngine::objectCreated,
        &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);

    const auto getQmlMainWindow = [&]() -> QObject*
    {
        for (auto* obj : qmlEngine.rootObjects()) {
            if (obj->objectName() == "MainWindow")
            {
                return obj;
            }
        }

        return nullptr;
    };

    if (auto* qmlMainWindow = getQmlMainWindow()) {
        QObject::connect(qmlMainWindow,SIGNAL(startGame()), coreModel.get(), SLOT(onUpdateState()));
        QObject::connect(qmlMainWindow,SIGNAL(requestAccepted(QString)),coreModel->requestModel(),SLOT(onRequestAppend(QString)));
        QObject::connect(qmlMainWindow,SIGNAL(diceRoll()),coreModel->diceModel(),SLOT(onGenerate()));
        QObject::connect(qmlMainWindow,SIGNAL(diceStop()),coreModel->diceModel(),SLOT(onStop()));
        QObject::connect(qmlMainWindow,SIGNAL(coinTurn()),coreModel->coinModel(),SLOT(onGenerate()));
        QObject::connect(qmlMainWindow,SIGNAL(coinStop()),coreModel->coinModel(),SLOT(onStop()));
        QObject::connect(qmlMainWindow,SIGNAL(actionBack()),coreModel.get(),SLOT(onUpdateState()));
        QObject::connect(qmlMainWindow,SIGNAL(actionNext()),coreModel.get(),SLOT(onUpdateState()));

        QObject::connect(coreModel.get(),SIGNAL(stateChanged()),qmlMainWindow,SLOT(onCoreStateChanged()));
    } else {
        QCoreApplication::exit(-1);
    }

	return app.exec();
}
