/******************************************************************************
**
** File      Randomizer.h
** Author    Andrii Sydorenko
**
******************************************************************************/

#pragma once

#include <random>

namespace Nfs
{

namespace utils
{

class Randomizer
{
public:
    explicit Randomizer(unsigned min = 0, unsigned max = std::numeric_limits<unsigned>::max());
    ~Randomizer() = default;

    unsigned generateValue();
    unsigned getMinValue() const;
    unsigned getMaxValue() const;

private:

	std::random_device _dev;
	std::seed_seq _seed;
	std::mt19937 _mt19937;
    std::uniform_int_distribution<unsigned> _dist;
};

}

}   // namespace zuma
