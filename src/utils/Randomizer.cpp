/******************************************************************************
**
** File      Randomizer.cpp
** Author    Andrii Sydorenko
**
******************************************************************************/

#include "Randomizer.h"

namespace Nfs
{

namespace utils
{

Randomizer::Randomizer(unsigned min, unsigned max)
    : _seed{_dev(),_dev(),_dev(),_dev()}
    , _mt19937{_seed}
    , _dist{min,max}
{
}

unsigned Randomizer::generateValue()
{
    return _dist(_mt19937);
}

unsigned int Randomizer::getMinValue() const
{
    return _dist.min();
}

unsigned int Randomizer::getMaxValue() const
{
    return _dist.max();
}

}	// namespace utils

}   // namespace Nfs
